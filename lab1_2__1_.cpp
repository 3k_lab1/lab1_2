#include <limits>
#include <iostream>

using namespace std;

int main() 
{
  cout << "Максимальное и минимальное значение для int: \n"//Диапазон предельных значений меняется от –32768 до 32767 (при 2 байтах) или от −2147483 648 до 2147483647 (при 4 байтах)
       <<"max: "<< numeric_limits<int>::max() << "\n"
       <<"min: "<< numeric_limits<int>::min() << "\n";
  cout << "Максимальное и минимальное значение для short: \n"//Диапазон предельных значений меняется от –32768 до 32767
       <<"max: "<< numeric_limits<short>::max() << "\n"
       <<"min: "<< numeric_limits<short>::min() << "\n";
  cout << "Максимальное и минимальное значение для unsigned short: \n"//Диапазон предельных значений меняется от 0 до 65535
       <<"max: "<< numeric_limits<unsigned short>::max() << "\n"
       <<"min: "<< numeric_limits<unsigned short>::min() << "\n";
  cout << "Максимальное и минимальное значение для unsigned: \n"//Диапазон предельных значений меняется от от 0 до 65535 (для 2 байт), либо от 0 до 4 294 967 295 (для 4 байт)
       <<"max: "<< numeric_limits<unsigned>::max() << "\n"
       <<"min: "<< numeric_limits<unsigned>::min() << "\n";
  cout << "Максимальное и минимальное значение для long: \n"//Диапазон предельных значений меняется от −2 147 483 648 до 2 147 483 647
       <<"max: "<< numeric_limits<long>::max() << "\n"
       <<"min: "<< numeric_limits<long>::min() << "\n";
  cout << "Максимальное и минимальное значение для unsigned long: \n"//Диапазон предельных значений меняется от  0 до 4 294 967 295
       <<"max: "<< numeric_limits<unsigned long>::max() << "\n"
       <<"min: "<< numeric_limits<unsigned long>::min() << "\n";
  cout << "Максимальное и минимальное значение для long long: \n"//Диапазон предельных значений меняется от −9223372036854775808 до +9223372036854775807
       <<"max: "<< numeric_limits<long long>::max() << "\n"
       <<"min: "<< numeric_limits<long long>::min() << "\n";
  cout << "Максимальное и минимальное значение для unsigned long long: \n"//Диапазон предельных значений меняется от 0 до 18446744073709551615
       <<"max: "<< numeric_limits<unsigned long long>::max() << "\n"
       <<"min: "<< numeric_limits<unsigned long long>::min() << "\n";
  cout << "Максимальное и минимальное значение для float: \n"//Диапазон предельных значений меняется от +/- 3.4E-38 до 3.4E+38.
       <<"max: "<< numeric_limits<float>::max() << "\n"
       <<"min: "<< numeric_limits<float>::min() << "\n";
  cout << "Максимальное и минимальное значение для double: \n"//Диапазон предельных значений меняется от +/- 1.7E-308 до 1.7E+308
       <<"max: "<< numeric_limits<double>::max() << "\n"
       <<"min: "<< numeric_limits<double>::min() << "\n";
  cout << "Максимальное и минимальное значение для long double: \n"//В зависимости от размера занимаемой памяти может отличаться диапазон допустимых значений.
       <<"max: "<< numeric_limits<long double>::max() << "\n"
       <<"min: "<< numeric_limits<long double>::min() << "\n";
}